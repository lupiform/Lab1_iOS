//
//  ViewController.swift
//  Hello Single View
//
//  Created by Camilla Jensen on 2017-10-25.
//  Copyright © 2017 Camilla. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func buttonTap(_ sender: UIButton) {
        print("hello button")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

